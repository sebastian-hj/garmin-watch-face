# Analog Plus

![Analog Plus](analog_plus.png)

## Features

- 24-hour dial
- Temperature
- Weather icons
- Sunrise/sunset ring
- Date number
- Number of notifications

## Set-up

- Download SDK: https://developer.garmin.com/connect-iq/sdk/

### Transfering to Watch

- "Monkey C: Build Current Project"
  - Watchface should be generated in `.\bin\` directory with `.prg` extension
- Copy to `.prg` file to `GARMIN\Apps\` directory

## Licensing

- [Weather Icons](https://github.com/erikflowers/weather-icons) licensed under [SIL OFL 1.1](http://scripts.sil.org/OFL)
