using Toybox.Graphics;
using Toybox.Position;
using Toybox.System;
using Toybox.Time;
using Toybox.Time.Gregorian;
using Toybox.Weather;
using Toybox.WatchUi;
import Toybox.Application;
import Toybox.Graphics;
import Toybox.Lang;
import Toybox.WatchUi;

class analog_plusView extends WatchUi.WatchFace {

    // Constants
    // Time
    hidden const NUMBER_OF_HOURS = 24;
    hidden const MINUTES_PER_HOUR = 60;
    hidden const MINUTES_PER_DAY = NUMBER_OF_HOURS * MINUTES_PER_HOUR;
    // Hands
    hidden const HAND_HOUR_LENGTH_PERC = 0.45f;
    hidden const HAND_MINUTES_LENGTH_PERC = 0.60f;
    hidden const HAND_CENTER_RADIUS = 8;
    hidden const HAND_LINE_PEN_WIDTH = 12;
    // Offsets
    hidden const OFFSET_HAND_HOUR_24H = -90.0f;
    hidden const OFFSET_HAND_HOUR_12H = 90.0f;
    hidden const OFFSET_HAND_MINUTES = 90.0f;
    hidden const OFFSET_SUNRISE_SUNSET = 270.0f;
    // Sunrise sunset Arc
    hidden const SUNRISE_SUNSET_COLOR_TYPE = 0; // 0: solid, 1: gradient
    // Font
    hidden const FONT_NAME = "BionicBold";
    hidden const FONT_SIZE_HAND_NUMBER = 16;
    hidden const FONT_SIZE_TEMPERATURE = 24;
    hidden const FONT_SIZE_DATE = 24;
    hidden const FONT_SIZE_NOTIFICATION_COUNT = 24;

    // Dimensions
    hidden var radius as Number or Null;
    hidden var centerX as Number or Null;
    hidden var centerY as Number or Null;
    // Date and time
    hidden var time;
    hidden var timeNow;
    hidden var today;
    // Sunrise and sunset
    hidden var sunrise;
    hidden var sunset;
    // Weather
    hidden var currentConditions;
    hidden var currentConditionsLastKnown;
    // Device settings
    hidden var deviceSettings;

    // Icons (API Level 3.2.0)
    enum ConditionList {
        CLEAR,
        PARTLY_CLOUDY,
        MOSTLY_CLOUDY,
        RAIN,
        SNOW,
        WINDY,
        THUNDERSTORMS,
        WINTRY_MIX,
        FOG,
        HAZY,
        HAIL,
        SCATTERED_SHOWERS,
        SCATTERED_THUNDERSTORMS,
        UNKNOWN_PRECIPITATION,
        LIGHT_RAIN,
        HEAVY_RAIN,
        LIGHT_SNOW,
        HEAVY_SNOW,
        LIGHT_RAIN_SNOW,
        HEAVY_RAIN_SNOW,
        CLOUDY,
        RAIN_SNOW,
        PARTLY_CLEAR,
        MOSTLY_CLEAR,
        LIGHT_SHOWERS,
        SHOWERS,
        HEAVY_SHOWERS,
        CHANCE_OF_SHOWERS,
        CHANCE_OF_THUNDERSTORMS,
        MIST,
        DUST,
        DRIZZLE,
        TORNADO,
        SMOKE,
        ICE,
        SAND,
        SQUALL,
        SANDSTORM,
        VOLCANIC_ASH,
        HAZE,
        FAIR,
        HURRICANE,
        TROPICAL_STORM,
        CHANCE_OF_SNOW,
        CHANCE_OF_RAIN_SNOW,
        CLOUDY_CHANCE_OF_RAIN,
        CLOUDY_CHANCE_OF_SNOW,
        CLOUDY_CHANCE_OF_RAIN_SNOW,
        FLURRIES,
        FREEZING_RAIN,
        SLEET,
        ICE_SNOW,
        THIN_CLOUDS,
        UNKNOWN,
    }
    private var conditionDayIcons as Array<BitmapType>;
    private var conditionNightIcons as Array<BitmapType>;

    //
    // Helper Functions
    //

    // Convert time to hour angle (in radians)
    hidden function timeToHourAngle(time as Time.Gregorian.Info, angleOffset as Float) {
        // Get total time in minutes
        var minutes = time.min + (time.hour * MINUTES_PER_HOUR);

        // Get clockwise angle as percentage of 360 degrees
        var hourAngle = -360.0f * (minutes.toFloat() / MINUTES_PER_DAY);

        // Add start of hour offset
        hourAngle = hourAngle + angleOffset;

        // Convert to radians
        hourAngle = Math.toRadians(-1.0 * hourAngle);

        return hourAngle;
    }

    // Convert time to minutes angle (in radians)
    hidden function timeToMinutesAngle(time as System.ClockTime, angleOffset as Float) {
        // Get clockwise angle as percentage of 360 degrees
        var minutesAngle = -360.0f * (time.min.toFloat() / 60.0f);

        // Add start of hour offset
        minutesAngle = minutesAngle + angleOffset;

        // Convert to radians
        minutesAngle = Math.toRadians(-1.0 * minutesAngle);

        return minutesAngle;
    }

    hidden function drawHand(dc as Graphics.Dc, angle as Float, handLengthPercentage as Float, handColor as Graphics.ColorValue) {
        // Calculate sine and cosine for current angle
        var cosAngle = Math.cos(angle);
        var sinAngle = Math.sin(angle);

        // Calculate length of hand
        var handLength = (radius * handLengthPercentage).toNumber();

        // Calculate end points of hand
        // (Start points are centerX and centerY)
        var handEndX = centerX + (cosAngle * handLength);
        var handEndY = centerY + (sinAngle * handLength);

        // Set color for hand
        dc.setColor(handColor, handColor);

        // Create circles at beginning of hand
        dc.fillCircle(centerX, centerY, HAND_CENTER_RADIUS);

        // Set line width
        dc.setPenWidth(HAND_LINE_PEN_WIDTH);
        dc.drawLine(centerX, centerY, handEndX, handEndY);
    }

    //
    // Main Functions
    //

    // Clear the screen
    hidden function clear(dc as Graphics.Dc) {
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();

        // Set anti-alias
        dc.setAntiAlias(true);
    }

    // Get global variables
    hidden function getGlobalVariables() {
        // Get current time (in System.ClockTime type)
        time = System.getClockTime();
        // Get time now
        timeNow = Time.now();
        // Get today
        today = Time.today();

        // Get weather
        currentConditions = Weather.getCurrentConditions();
        if (currentConditions != null) {
            currentConditionsLastKnown = currentConditions;
        }

        // Get device settings
        deviceSettings = System.getDeviceSettings();
    }

    // Draw sunrise and sunset arc
    hidden function drawSunriseSunsetArc(dc as Graphics.Dc) {
        var currentLocation;

        if (currentConditions != null) {
            currentLocation = currentConditions.observationLocationPosition;
        }
        else if (currentConditionsLastKnown != null) {
            currentLocation = currentConditionsLastKnown.observationLocationPosition;
        }
        else {
            return;
        }

        // // Test location: Oakland
        // var currentLocation = new Position.Location(
        //     {
        //         :latitude => 37.804444,
        //         :longitude => -122.270833,
        //         :format => :degrees
        //     }
        // );

        if ((currentLocation != null) && (NUMBER_OF_HOURS == 24)) {
            // Get sunrise and sunset time for current location
            sunrise = Weather.getSunrise(currentLocation, today);
            sunset = Weather.getSunset(currentLocation, today);

            // Convert sunrise and sunset time to Gregorian format
            var sunriseTime = Gregorian.info(sunrise, Time.FORMAT_SHORT);
            var sunsetTime = Gregorian.info(sunset, Time.FORMAT_SHORT);

            // Convert sunrise and sunset time to counter-clockwise degrees
            var sunriseAngle = -1.0f * Math.toDegrees(timeToHourAngle(sunriseTime, OFFSET_SUNRISE_SUNSET));
            var sunsetAngle = -1.0f * Math.toDegrees(timeToHourAngle(sunsetTime, OFFSET_SUNRISE_SUNSET));

            // Check color type
            if (SUNRISE_SUNSET_COLOR_TYPE == 1) {
                // Set color based on sunrise to sunset color palette
                // https://www.color-hex.com/color-palette/24135
                // var colorsSunriseToSunset = [
                //     (181 << 16) | (214 << 8) | 224,
                //     (199 << 16) | (225 << 8) | 229,
                //     (255 << 16) | (239 << 8) | 122,
                //     (247 << 16) | (193 << 8) | 106,
                //     (244 << 16) | (106 << 8) | 85,
                // ];

                // Blend of 
                // https://colorkit.co/palette/0033a0-1c49ab-375fb5-6d8ac9-88a0d3-a3b5dd-becbe7-d9e0f1/
                // and 
                // https://colorkit.co/palette/ef9f1c-ef8713-e68011-dd6911-cd4811-c14113-aa3616-8a3220/
                // var colorsSunriseToSunset = [
                //     0x0033a0,
                //     0x1c49ab,
                //     0x375fb5,
                //     0x6d8ac9,
                //     0x88a0d3,
                //     0xa3b5dd,
                //     0xbecbe7,
                //     0xd9e0f1,
                //     0xef9f1c,
                //     0xef8713,
                //     0xe68011,
                //     0xdd6911,
                //     0xcd4811,
                //     0xc14113,
                //     0xaa3616,
                //     0x8a3220,
                // ];

                // Gemini...
                var colorsSunriseToSunset = [
                    0xFFE599,
                    0xFFD700,
                    0xADD8E6,
                    0x40E0D0,
                    0xE6E6FA,
                    0xD7AFD7,
                    0xFF7F50,
                    0x800020,
                ];

                var colorsSunriseToSunsetLength = colorsSunriseToSunset.size();

                var angleStep = (sunsetAngle - sunriseAngle) / colorsSunriseToSunsetLength;
                for (var i = 0; i < colorsSunriseToSunsetLength; i++) {
                    // Set color
                    dc.setColor(colorsSunriseToSunset[i], Graphics.COLOR_TRANSPARENT);
                    
                    // Get start and end angles
                    var angleStart = sunriseAngle + (angleStep * i);
                    var angleEnd = sunriseAngle + (angleStep * (i + 1));

                    // Draw arc
                    dc.drawArc(centerX, centerY, radius, Graphics.ARC_CLOCKWISE, angleStart, angleEnd);
                    dc.drawArc(centerX, centerY, radius - 0.5, Graphics.ARC_CLOCKWISE, angleStart, angleEnd);
                }
            }
            else {
                // Set color and draw sunrise to sunset arc
                var red = 0;
                var green = 64;
                var blue = 192;
                var customColor = (red << 16) | (green << 8) | blue;
                dc.setColor(customColor, Graphics.COLOR_TRANSPARENT);
                dc.drawArc(centerX, centerY, radius, Graphics.ARC_CLOCKWISE, sunriseAngle, sunsetAngle);
                dc.drawArc(centerX, centerY, radius - 0.5, Graphics.ARC_CLOCKWISE, sunriseAngle, sunsetAngle);
            }
        }
    }

    // Draw hours dial
    hidden function drawHoursDial(dc as Graphics.Dc) {
        // Set number of hours
        var hourNumber = NUMBER_OF_HOURS;
        var radialSegments = 360.0f / hourNumber.toFloat();

        // Calculate radial start and end distance for line
        var radialEnd = radius + 1;
        var radialStart = radialEnd - 10;

        // Set line color and width
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
        dc.setPenWidth(3);

        // Iterate over number of hours
        for (var i = 0; i < hourNumber; i += 1) {
            // Calculate angle for current segment
            var angle = Math.toRadians(radialSegments * i.toFloat());

            // Calculate sine and cosine for current angle
            var cosAngle = Math.cos(angle);
            var sinAngle = Math.sin(angle);

            // Calculate start and end coordinates of line
            var x1 = centerX + (radialStart * cosAngle);
            var y1 = centerY + (radialStart * sinAngle);
            var x2 = centerX + (radialEnd * cosAngle);
            var y2 = centerY + (radialEnd * sinAngle);

            // Draw line
            dc.drawLine(x1, y1, x2, y2);
        }
    }

    // Draw minutes hand
    hidden function drawMinutesHand(dc as Graphics.Dc) {
        // Convert current minutes to angle
        var minutesAngle = timeToMinutesAngle(time, OFFSET_HAND_MINUTES);

        // Draw hand
        drawHand(dc, minutesAngle, HAND_MINUTES_LENGTH_PERC, Graphics.COLOR_LT_GRAY);
    }

    // Draw hours hand
    hidden function drawHoursHand(dc as Graphics.Dc) {
        // Convert current hour to angle
        var hourAngle;
        if (NUMBER_OF_HOURS == 24) {
            hourAngle = timeToHourAngle(time, OFFSET_HAND_HOUR_24H);
        }
        else if (NUMBER_OF_HOURS == 12) {
            time.hour = time.hour - 12;
            hourAngle = timeToHourAngle(time, OFFSET_HAND_HOUR_12H);
        }
        else {
            return;
        }

        // Draw hand
        drawHand(dc, hourAngle, HAND_HOUR_LENGTH_PERC, Graphics.COLOR_RED);
    }

    // Draw hand numbers
    hidden function drawHandNumbers(dc as Graphics.Dc) {
        // Declare text variables
        var topText = "";
        var rightText = "";
        var bottomText = "";
        var leftText = "";

        // Define text depending on number of hours
        if (NUMBER_OF_HOURS == 24) {
            topText = "12";
            rightText = "18";
            bottomText = "24";
            leftText = "6";
        }
        else if (NUMBER_OF_HOURS == 12) {
            topText = "12";
            rightText = "3";
            bottomText = "6";
            leftText = "9";
        }

        // Set vector font
        // See: https://developer.garmin.com/connect-iq/reference-guides/devices-reference/
        // var fontName = "BionicBold"; // G
        // var fontName = "KosugiRegular"; // NG
        // var fontName = "NanumGothicRegular"; // Ok
        // var fontName = "NotoSansSCMedium"; // Ok
        // var fontName = "PridiRegularGarmin"; // G
        // var fontName = "RobotoCondensedBold"; // G
        // var fontName = "RobotoCondensedRegular"; // G
        // var fontName = "SakkalMajallaBold"; // Small
        // var fontName = "SakkalMajallaRoman"; // Small
        // var fontName = "Swiss721Bold"; // Big
        // var fontName = "Swiss721Regular"; // Big

        // Custom font
        // https://developer.garmin.com/connect-iq/connect-iq-faq/how-do-i-use-custom-fonts/

        var vectorFont = Graphics.getVectorFont({:face => FONT_NAME, :size => FONT_SIZE_HAND_NUMBER});

        // Draw top hour number
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
        dc.drawAngledText(centerX, centerY - (radius - 22), vectorFont, topText, Graphics.TEXT_JUSTIFY_CENTER, 0);

        // Draw bottom hour number
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
        dc.drawAngledText(centerX, centerY + (radius - 14), vectorFont, bottomText, Graphics.TEXT_JUSTIFY_CENTER, 0);

        // Draw left hour number
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
        dc.drawAngledText(centerX - (radius - 16), centerY + 5, vectorFont, leftText, Graphics.TEXT_JUSTIFY_CENTER, 0);

        // Draw right hour number
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_WHITE);
        dc.drawAngledText(centerX + (radius - 18), centerY + 5, vectorFont, rightText, Graphics.TEXT_JUSTIFY_CENTER, 0);
    }

    // Draw temperature
    hidden function drawTemperature(dc as Graphics.Dc) {
        if ((currentConditions == null) || (currentConditions.temperature == null)) {
            return;
        }

        var temperature = currentConditions.temperature.toFloat();
        if (deviceSettings.temperatureUnits == 1) {
            temperature = (currentConditions.temperature * (9.0f / 5.0f)) + 32.0f;
        }

        // Prepare to round
        if (temperature < 0.0f) {
            temperature = temperature - 0.5f;
        }
        else {
            temperature = temperature + 0.5f;
        }
        // Round properly
        temperature = temperature.toNumber();

        // Convert temperature to string
        var temperatureStr = temperature.toString() + "°";

        var vectorFont = Graphics.getVectorFont({:face => FONT_NAME, :size => FONT_SIZE_TEMPERATURE});

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawAngledText(centerX - 40, centerY - 48, vectorFont, temperatureStr, Graphics.TEXT_JUSTIFY_CENTER, 0);
    }

    // Draw weather condition as text
    hidden function drawWeatherText(dc as Graphics.Dc) {
        if ((currentConditions == null) || (currentConditions.temperature == null)) {
            return;
        }

        // API Level 3.2.0
        var conditionList = [
            "Clear",
            "Partly cloudy",
            "Mostly cloudy",
            "Rain",
            "Snow",
            "Windy",
            "Thunderstorms",
            "Wintry mix",
            "Fog",
            "Hazy",
            "Hail",
            "Scattered showers",
            "Scattered thunderstorms",
            "Unknown precipitation",
            "Light rain",
            "Heavy rain",
            "Light snow",
            "Heavy snow",
            "Light rain snow",
            "Heavy rain snow",
            "Cloudy",
            "Rain snow",
            "Partly clear",
            "Mostly clear",
            "Light showers",
            "Showers",
            "Heavy showers",
            "Chance of showers",
            "Chance of thunderstorms",
            "Mist",
            "Dust",
            "Drizzle",
            "Tornado",
            "Smoke",
            "Ice",
            "Sand",
            "Squall",
            "Sandstorm",
            "Volcanic ash",
            "Haze",
            "Fair",
            "Hurricane",
            "Tropical storm",
            "Chance of snow",
            "Chance of rain snow",
            "Cloudy chance of rain",
            "Cloudy chance of snow",
            "Cloudy chance of rain snow",
            "Flurries",
            "Freezing rain",
            "Sleet",
            "Ice snow",
            "Thin clouds",
            "Unknown ",
        ];

        var condition = currentConditions.condition;
        var conditionStr = conditionList[condition];

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawText(centerX - 40, centerY - 42, Graphics.FONT_XTINY, conditionStr, Graphics.TEXT_JUSTIFY_CENTER);
    }

    // Draw weather condition as SVG icon
    hidden function drawWeatherIcon(dc as Graphics.Dc) {
        if ((currentConditions == null) || (currentConditions.temperature == null)) {
            return;
        }

        var condition = currentConditions.condition;
        var conditionIcon;

        if ((sunrise != null) && (sunset != null)) {
            if ((timeNow.greaterThan(sunrise)) && (timeNow.lessThan(sunset))) {
                conditionIcon = conditionDayIcons[condition];
            }
            else {
                conditionIcon = conditionNightIcons[condition];
            }
        }
        else {
            conditionIcon = conditionDayIcons[condition];
        }

        // Draw the icon
        dc.drawBitmap(centerX - 65, centerY - 48, conditionIcon);
    }

    // Draw date number
    hidden function drawCurrentDate(dc as Graphics.Dc) {
        var todayInfo = Gregorian.info(today, Time.FORMAT_SHORT);

        var vectorFont = Graphics.getVectorFont({:face => FONT_NAME, :size => FONT_SIZE_DATE});

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawAngledText(centerX + 40, centerY - 40, vectorFont, todayInfo.day.toString(), Graphics.TEXT_JUSTIFY_CENTER, 0);
    }

    // Draw notification count (if any)
    hidden function drawNotificationCount(dc as Graphics.Dc) {
        // https://developer.garmin.com/connect-iq/api-docs/Toybox/System/DeviceSettings.html#notificationCount-var

        var notificationCount = deviceSettings.notificationCount;

        if (notificationCount != 0) {
            var vectorFont = Graphics.getVectorFont({:face => FONT_NAME, :size => FONT_SIZE_NOTIFICATION_COUNT});

            dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
            dc.drawAngledText(centerX, centerY + 60, vectorFont, notificationCount.toString(), Graphics.TEXT_JUSTIFY_CENTER, 0);
        }
    }

    function initialize() {
        WatchFace.initialize();

        // Initialize weather condition day icons
        conditionDayIcons = new Array<BitmapType>[UNKNOWN + 1];

        conditionDayIcons[CLEAR] = Application.loadResource(Rez.Drawables.day_clear);
        conditionDayIcons[PARTLY_CLOUDY] = Application.loadResource(Rez.Drawables.day_partly_cloudy);
        conditionDayIcons[MOSTLY_CLOUDY] = Application.loadResource(Rez.Drawables.day_mostly_cloudy);
        conditionDayIcons[RAIN] = Application.loadResource(Rez.Drawables.day_rain);
        conditionDayIcons[SNOW] = Application.loadResource(Rez.Drawables.day_snow);
        conditionDayIcons[WINDY] = Application.loadResource(Rez.Drawables.day_windy);
        conditionDayIcons[THUNDERSTORMS] = Application.loadResource(Rez.Drawables.day_thunderstorms);
        conditionDayIcons[WINTRY_MIX] = Application.loadResource(Rez.Drawables.day_wintry_mix);
        conditionDayIcons[FOG] = Application.loadResource(Rez.Drawables.day_fog);
        conditionDayIcons[HAZY] = Application.loadResource(Rez.Drawables.day_hazy);
        conditionDayIcons[HAIL] = Application.loadResource(Rez.Drawables.day_hail);
        conditionDayIcons[SCATTERED_SHOWERS] = Application.loadResource(Rez.Drawables.day_scattered_showers);
        conditionDayIcons[SCATTERED_THUNDERSTORMS] = Application.loadResource(Rez.Drawables.day_scattered_thunderstorms);
        conditionDayIcons[UNKNOWN_PRECIPITATION] = Application.loadResource(Rez.Drawables.day_unknown_precipitation);
        conditionDayIcons[LIGHT_RAIN] = Application.loadResource(Rez.Drawables.day_light_rain);
        conditionDayIcons[HEAVY_RAIN] = Application.loadResource(Rez.Drawables.day_heavy_rain);
        conditionDayIcons[LIGHT_SNOW] = Application.loadResource(Rez.Drawables.day_light_snow);
        conditionDayIcons[HEAVY_SNOW] = Application.loadResource(Rez.Drawables.day_heavy_snow);
        conditionDayIcons[LIGHT_RAIN_SNOW] = Application.loadResource(Rez.Drawables.day_light_rain_snow);
        conditionDayIcons[HEAVY_RAIN_SNOW] = Application.loadResource(Rez.Drawables.day_heavy_rain_snow);
        conditionDayIcons[CLOUDY] = Application.loadResource(Rez.Drawables.day_cloudy);
        conditionDayIcons[RAIN_SNOW] = Application.loadResource(Rez.Drawables.day_rain_snow);
        conditionDayIcons[PARTLY_CLEAR] = Application.loadResource(Rez.Drawables.day_partly_clear);
        conditionDayIcons[MOSTLY_CLEAR] = Application.loadResource(Rez.Drawables.day_mostly_clear);
        conditionDayIcons[LIGHT_SHOWERS] = Application.loadResource(Rez.Drawables.day_light_showers);
        conditionDayIcons[SHOWERS] = Application.loadResource(Rez.Drawables.day_showers);
        conditionDayIcons[HEAVY_SHOWERS] = Application.loadResource(Rez.Drawables.day_heavy_showers);
        conditionDayIcons[CHANCE_OF_SHOWERS] = Application.loadResource(Rez.Drawables.day_chance_of_showers);
        conditionDayIcons[CHANCE_OF_THUNDERSTORMS] = Application.loadResource(Rez.Drawables.day_chance_of_thunderstorms);
        conditionDayIcons[MIST] = Application.loadResource(Rez.Drawables.day_mist);
        conditionDayIcons[DUST] = Application.loadResource(Rez.Drawables.day_dust);
        conditionDayIcons[DRIZZLE] = Application.loadResource(Rez.Drawables.day_drizzle);
        conditionDayIcons[TORNADO] = Application.loadResource(Rez.Drawables.day_tornado);
        conditionDayIcons[SMOKE] = Application.loadResource(Rez.Drawables.day_smoke);
        conditionDayIcons[ICE] = Application.loadResource(Rez.Drawables.day_ice);
        conditionDayIcons[SAND] = Application.loadResource(Rez.Drawables.day_sand);
        conditionDayIcons[SQUALL] = Application.loadResource(Rez.Drawables.day_squall);
        conditionDayIcons[SANDSTORM] = Application.loadResource(Rez.Drawables.day_sandstorm);
        conditionDayIcons[VOLCANIC_ASH] = Application.loadResource(Rez.Drawables.day_volcanic_ash);
        conditionDayIcons[HAZE] = Application.loadResource(Rez.Drawables.day_haze);
        conditionDayIcons[FAIR] = Application.loadResource(Rez.Drawables.day_fair);
        conditionDayIcons[HURRICANE] = Application.loadResource(Rez.Drawables.day_hurricane);
        conditionDayIcons[TROPICAL_STORM] = Application.loadResource(Rez.Drawables.day_tropical_storm);
        conditionDayIcons[CHANCE_OF_SNOW] = Application.loadResource(Rez.Drawables.day_chance_of_snow);
        conditionDayIcons[CHANCE_OF_RAIN_SNOW] = Application.loadResource(Rez.Drawables.day_chance_of_rain_snow);
        conditionDayIcons[CLOUDY_CHANCE_OF_RAIN] = Application.loadResource(Rez.Drawables.day_cloudy_chance_of_rain);
        conditionDayIcons[CLOUDY_CHANCE_OF_SNOW] = Application.loadResource(Rez.Drawables.day_cloudy_chance_of_snow);
        conditionDayIcons[CLOUDY_CHANCE_OF_RAIN_SNOW] = Application.loadResource(Rez.Drawables.day_cloudy_chance_of_rain_snow);
        conditionDayIcons[FLURRIES] = Application.loadResource(Rez.Drawables.day_flurries);
        conditionDayIcons[FREEZING_RAIN] = Application.loadResource(Rez.Drawables.day_freezing_rain);
        conditionDayIcons[SLEET] = Application.loadResource(Rez.Drawables.day_sleet);
        conditionDayIcons[ICE_SNOW] = Application.loadResource(Rez.Drawables.day_ice_snow);
        conditionDayIcons[THIN_CLOUDS] = Application.loadResource(Rez.Drawables.day_thin_clouds);
        conditionDayIcons[UNKNOWN] = Application.loadResource(Rez.Drawables.day_unknown);

        // Initialize weather condition night icons
        conditionNightIcons = new Array<BitmapType>[UNKNOWN + 1];

        conditionNightIcons[CLEAR] = Application.loadResource(Rez.Drawables.night_clear);
        conditionNightIcons[PARTLY_CLOUDY] = Application.loadResource(Rez.Drawables.night_partly_cloudy);
        conditionNightIcons[MOSTLY_CLOUDY] = Application.loadResource(Rez.Drawables.night_mostly_cloudy);
        conditionNightIcons[RAIN] = Application.loadResource(Rez.Drawables.night_rain);
        conditionNightIcons[SNOW] = Application.loadResource(Rez.Drawables.night_snow);
        conditionNightIcons[WINDY] = Application.loadResource(Rez.Drawables.night_windy);
        conditionNightIcons[THUNDERSTORMS] = Application.loadResource(Rez.Drawables.night_thunderstorms);
        conditionNightIcons[WINTRY_MIX] = Application.loadResource(Rez.Drawables.night_wintry_mix);
        conditionNightIcons[FOG] = Application.loadResource(Rez.Drawables.night_fog);
        conditionNightIcons[HAZY] = Application.loadResource(Rez.Drawables.night_hazy);
        conditionNightIcons[HAIL] = Application.loadResource(Rez.Drawables.night_hail);
        conditionNightIcons[SCATTERED_SHOWERS] = Application.loadResource(Rez.Drawables.night_scattered_showers);
        conditionNightIcons[SCATTERED_THUNDERSTORMS] = Application.loadResource(Rez.Drawables.night_scattered_thunderstorms);
        conditionNightIcons[UNKNOWN_PRECIPITATION] = Application.loadResource(Rez.Drawables.night_unknown_precipitation);
        conditionNightIcons[LIGHT_RAIN] = Application.loadResource(Rez.Drawables.night_light_rain);
        conditionNightIcons[HEAVY_RAIN] = Application.loadResource(Rez.Drawables.night_heavy_rain);
        conditionNightIcons[LIGHT_SNOW] = Application.loadResource(Rez.Drawables.night_light_snow);
        conditionNightIcons[HEAVY_SNOW] = Application.loadResource(Rez.Drawables.night_heavy_snow);
        conditionNightIcons[LIGHT_RAIN_SNOW] = Application.loadResource(Rez.Drawables.night_light_rain_snow);
        conditionNightIcons[HEAVY_RAIN_SNOW] = Application.loadResource(Rez.Drawables.night_heavy_rain_snow);
        conditionNightIcons[CLOUDY] = Application.loadResource(Rez.Drawables.night_cloudy);
        conditionNightIcons[RAIN_SNOW] = Application.loadResource(Rez.Drawables.night_rain_snow);
        conditionNightIcons[PARTLY_CLEAR] = Application.loadResource(Rez.Drawables.night_partly_clear);
        conditionNightIcons[MOSTLY_CLEAR] = Application.loadResource(Rez.Drawables.night_mostly_clear);
        conditionNightIcons[LIGHT_SHOWERS] = Application.loadResource(Rez.Drawables.night_light_showers);
        conditionNightIcons[SHOWERS] = Application.loadResource(Rez.Drawables.night_showers);
        conditionNightIcons[HEAVY_SHOWERS] = Application.loadResource(Rez.Drawables.night_heavy_showers);
        conditionNightIcons[CHANCE_OF_SHOWERS] = Application.loadResource(Rez.Drawables.night_chance_of_showers);
        conditionNightIcons[CHANCE_OF_THUNDERSTORMS] = Application.loadResource(Rez.Drawables.night_chance_of_thunderstorms);
        conditionNightIcons[MIST] = Application.loadResource(Rez.Drawables.night_mist);
        conditionNightIcons[DUST] = Application.loadResource(Rez.Drawables.night_dust);
        conditionNightIcons[DRIZZLE] = Application.loadResource(Rez.Drawables.night_drizzle);
        conditionNightIcons[TORNADO] = Application.loadResource(Rez.Drawables.night_tornado);
        conditionNightIcons[SMOKE] = Application.loadResource(Rez.Drawables.night_smoke);
        conditionNightIcons[ICE] = Application.loadResource(Rez.Drawables.night_ice);
        conditionNightIcons[SAND] = Application.loadResource(Rez.Drawables.night_sand);
        conditionNightIcons[SQUALL] = Application.loadResource(Rez.Drawables.night_squall);
        conditionNightIcons[SANDSTORM] = Application.loadResource(Rez.Drawables.night_sandstorm);
        conditionNightIcons[VOLCANIC_ASH] = Application.loadResource(Rez.Drawables.night_volcanic_ash);
        conditionNightIcons[HAZE] = Application.loadResource(Rez.Drawables.night_haze);
        conditionNightIcons[FAIR] = Application.loadResource(Rez.Drawables.night_fair);
        conditionNightIcons[HURRICANE] = Application.loadResource(Rez.Drawables.night_hurricane);
        conditionNightIcons[TROPICAL_STORM] = Application.loadResource(Rez.Drawables.night_tropical_storm);
        conditionNightIcons[CHANCE_OF_SNOW] = Application.loadResource(Rez.Drawables.night_chance_of_snow);
        conditionNightIcons[CHANCE_OF_RAIN_SNOW] = Application.loadResource(Rez.Drawables.night_chance_of_rain_snow);
        conditionNightIcons[CLOUDY_CHANCE_OF_RAIN] = Application.loadResource(Rez.Drawables.night_cloudy_chance_of_rain);
        conditionNightIcons[CLOUDY_CHANCE_OF_SNOW] = Application.loadResource(Rez.Drawables.night_cloudy_chance_of_snow);
        conditionNightIcons[CLOUDY_CHANCE_OF_RAIN_SNOW] = Application.loadResource(Rez.Drawables.night_cloudy_chance_of_rain_snow);
        conditionNightIcons[FLURRIES] = Application.loadResource(Rez.Drawables.night_flurries);
        conditionNightIcons[FREEZING_RAIN] = Application.loadResource(Rez.Drawables.night_freezing_rain);
        conditionNightIcons[SLEET] = Application.loadResource(Rez.Drawables.night_sleet);
        conditionNightIcons[ICE_SNOW] = Application.loadResource(Rez.Drawables.night_ice_snow);
        conditionNightIcons[THIN_CLOUDS] = Application.loadResource(Rez.Drawables.night_thin_clouds);
        conditionNightIcons[UNKNOWN] = Application.loadResource(Rez.Drawables.night_unknown);
    }

    // Load your resources here
    function onLayout(dc) {
        // Get device width and height
        var width = dc.getWidth();
        var height = dc.getHeight();

        // Calculate device center x and y
        centerX = width / 2;
        centerY = height / 2;

        // Define radius as smallest of x or y center
        radius = centerX > centerY ? centerY : centerX;
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
    }

    // Update the view
    function onUpdate(dc) {
        clear(dc);
        getGlobalVariables();
        drawSunriseSunsetArc(dc);
        drawHoursDial(dc);
        drawMinutesHand(dc);
        drawHoursHand(dc);
        drawHandNumbers(dc);
        drawTemperature(dc);
        // drawWeatherText(dc);
        drawWeatherIcon(dc);
        drawCurrentDate(dc);
        drawNotificationCount(dc);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() as Void {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() as Void {
    }

}
